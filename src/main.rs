mod cache_handler;
mod collage;

use axum::{routing::get, Router};
use dotenv::dotenv;
use reqwest::Client;
use std::error::Error;
use std::sync::OnceLock;
use tokio::fs;
use tokio::net::TcpListener;
use tower_http::services::fs::ServeDir;

pub static CLIENT: OnceLock<Client> = OnceLock::new();

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv().ok();
    assert!(std::env::var("API_KEY").is_ok());

    if !fs::try_exists("collages").await? {
        fs::create_dir("collages").await?
    };

    CLIENT.get_or_init(Client::new);

    tokio::task::spawn(cache_handler::cleanup_service());

    let app = Router::new()
        .fallback_service(ServeDir::new("static"))
        .nest_service("/collages/", ServeDir::new("collages"))
        .route("/collage", get(collage::collage));
    axum::serve(TcpListener::bind("0.0.0.0:1337").await?, app).await?;

    Ok(())
}
