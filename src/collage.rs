use crate::cache_handler;

use crate::CLIENT;
use ab_glyph::{FontRef, PxScale};
use axum::{extract::Query, http::StatusCode, response::IntoResponse};
use build_html::{Html, HtmlContainer};
use futures::future::try_join_all;
use image::codecs::png::PngEncoder;
use image::{GenericImage, ImageReader, RgbImage, Rgba};
use imageproc::drawing::draw_text_mut;
use reqwest::Client;
use serde::Deserialize;
use std::fs::FileTimes;
use std::path::PathBuf;
use std::time::SystemTime;
use std::{error::Error, io::Cursor};
use tokio::fs;

const FONT: &[u8] = include_bytes!("../res/font.ttf");
const PLACEHOLDER: &[u8] = include_bytes!("../res/placeholder.png");

pub async fn collage(Query(params): Query<Input>) -> Result<axum::response::Response, MyError> {
    let track_ammount = params.size * params.size;
    let api_key = std::env::var("API_KEY")?;
    let client = CLIENT.get_or_init(Client::new);
    let request: Response = client
        .get(format!(
            "http://ws.audioscrobbler.com/2.0/\
        ?method=user.gettopalbums\
        &user={}\
        &limit={}\
        &period={}\
        &api_key={}\
        &format=json",
            params.user, track_ammount, params.period, api_key
        ))
        .send()
        .await?
        .json()
        .await?;

    let albums = request.topalbums.album;

    let mut alt_text = format!(
        "colagem de capas de álbuns em uma grade quadrada com {} linhas e colunas.\n\
        na ordem das linhas, estão ordenadas as seguintes capas:\n",
        params.size
    );
    for album in &albums {
        let label = format!("\n{}\npor {}\n", album.name, album.artist.name);
        alt_text.push_str(&label);
    }

    let hash = cache_handler::gen_hash(&albums, params.names.is_some(), params.size);
    let hash_path = format!("collages/{hash}.png");
    match fs::try_exists(&hash_path).await {
        Ok(true) => {
            println!("{hash}: cache hit");
            let file = fs::File::open(&hash_path).await?;
            let metadata = file.metadata().await?;
            let times = FileTimes::new()
                .set_modified(metadata.modified()?)
                .set_accessed(SystemTime::now());
            file.into_std().await.set_times(times)?;
        }
        Ok(false) => {
            println!("{hash}: gerando imagem...");
            generate_image(&params, &albums, &hash_path, &hash).await?;
            println!("{hash}: gerado!");
        }
        Err(e) => return Err(e.into()),
    };

    let page = build_html::HtmlPage::new()
        .with_title("semaninha : )")
        .with_meta(vec![
            ("name", "viewport"),
            ("content", "width=device-width, initial-scale=1"),
        ])
        .with_stylesheet("collage.css")
        .with_image(hash_path, &alt_text)
        .with_preformatted(alt_text);

    Ok(axum::response::Html::from(page.to_html_string()).into_response())
}

async fn generate_image(
    params: &Input,
    albums: &Vec<Album>,
    location: &str,
    hash: &str,
) -> Result<(), MyError> {
    let size = 300 * params.size as u32;

    let mut full_image = RgbImage::new(size, size);
    let mut futures = Vec::new();

    for album in albums {
        let url = album
            .image
            .iter()
            .last()
            .ok_or("no image found...")?
            .text
            .clone();
        futures.push(get_cover(url, &album.name, &album.artist.name));
    }
    let covers = try_join_all(futures).await?;
    println!("{hash}: todas as capas baixadas.");
    for (i, (cover, name, artist)) in covers.iter().enumerate() {
        let mut image = ImageReader::new(Cursor::new(cover))
            .with_guessed_format()?
            .decode()?;
        if params.names.is_some() {
            let font = &FontRef::try_from_slice(FONT)?;
            draw_text_mut(
                &mut image,
                Rgba::from([0, 0, 0, 255]),
                6,
                6,
                PxScale::from(24.0),
                font,
                artist,
            );
            draw_text_mut(
                &mut image,
                Rgba::from([255, 255, 255, 255]),
                5,
                5,
                PxScale::from(24.0),
                font,
                artist,
            );
            draw_text_mut(
                &mut image,
                Rgba::from([0, 0, 0, 255]),
                6,
                30,
                PxScale::from(16.0),
                font,
                name,
            );
            draw_text_mut(
                &mut image,
                Rgba::from([255, 255, 255, 255]),
                5,
                29,
                PxScale::from(16.0),
                font,
                name,
            );
        }
        let (x, y) = (i as u32 / params.size as u32, i as u32 % params.size as u32);
        full_image.copy_from(&image.into_rgb8(), y * 300, x * 300)?;
    }
    println!("{hash}: codificando imagem.");
    let mut buffer = Vec::new();
    let encoder = PngEncoder::new_with_quality(
        Cursor::new(&mut buffer),
        image::codecs::png::CompressionType::Fast,
        image::codecs::png::FilterType::Adaptive,
    );
    full_image.write_with_encoder(encoder)?;

    let path = PathBuf::from(location);
    full_image.save(path)?;

    Ok(())
}

async fn get_cover<'a>(
    url: String,
    name: &'a String,
    artist: &'a String,
) -> Result<(axum::body::Bytes, &'a String, &'a String), reqwest::Error> {
    let client = CLIENT.get_or_init(Client::new);
    let bytes = match client.get(url).send().await {
        Ok(request) => request.bytes().await?,
        Err(_) => axum::body::Bytes::copy_from_slice(PLACEHOLDER),
    };
    Ok((bytes, name, artist))
}

#[derive(Deserialize)]
pub struct Input {
    user: String,
    size: u8,
    period: String,
    names: Option<String>,
}

#[derive(Deserialize, Debug)]
struct Response {
    topalbums: TopAlbum,
}

#[derive(Deserialize, Debug)]
struct TopAlbum {
    album: Vec<Album>,
}

#[derive(Deserialize, Debug)]
pub struct Album {
    name: String,
    artist: Artist,
    pub image: Vec<Image>,
}

#[derive(Deserialize, Debug)]
struct Artist {
    name: String,
}

#[derive(Deserialize, Debug)]
pub struct Image {
    #[serde(rename = "#text")]
    pub text: String,
}

// convert errors easily :p
pub struct MyError(Box<dyn Error>);
impl IntoResponse for MyError {
    fn into_response(self) -> axum::response::Response {
        (StatusCode::INTERNAL_SERVER_ERROR, self.0.to_string()).into_response()
    }
}
impl<E> From<E> for MyError
where
    E: Into<Box<dyn Error>>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}
