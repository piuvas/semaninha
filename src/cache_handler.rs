use crate::collage::Album;
use sha2::{Digest, Sha256};
use std::{path::Path, time::Duration};
use tokio::{fs, time::interval};

pub fn gen_hash(albums: &Vec<Album>, text: bool, size: u8) -> String {
    let mut hasher = Sha256::new();
    for album in albums {
        if let Some(image) = album.image.first() {
            hasher.update(&image.text);
        }
    }
    hasher.update(match text {
        true => &[0],
        false => &[1],
    });
    hasher.update([size]);
    let hash = hasher.finalize();
    hex::encode(hash)
}

pub async fn cleanup_service() -> ! {
    let cache_folder = Path::new("collages");
    let mut interval = interval(Duration::from_secs(60 * 60 * 24));
    loop {
        interval.tick().await;
        println!("limpando arquivos velhos...");
        match fs::read_dir(cache_folder).await {
            Ok(mut readdir) => {
                while let Ok(Some(direntry)) = readdir.next_entry().await {
                    if let Ok(metadata) = direntry.metadata().await {
                        if let Ok(accessed) = metadata.accessed() {
                            if let Ok(elapsed) = accessed.elapsed() {
                                if elapsed.as_secs() > (60 * 60 * 24 * 7) {
                                    match fs::remove_file(direntry.path()).await {
                                        Ok(_) => println!(
                                            "{:#?}: arquivo excluido",
                                            direntry.file_name()
                                        ),
                                        Err(e) => println!("{:#?}: {e}", direntry.file_name()),
                                    };
                                }
                            }
                        }
                    };
                }
            }
            Err(e) => println!("{e}"),
        }
    }
}
